package com.dedalus.restapi.dao;


import com.dedalus.restapi.model.Patient;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class PatientDAO {
    @PersistenceContext(unitName = "restapi_PU")
    private EntityManager em;

    public List getAll() {
        return em.createNamedQuery("Patient.findAll", Patient.class).getResultList();
    }

    public Patient findById(Long id) {
        return em.find(Patient.class, id);
    }

    public Patient findByPatientId(Long patientId) {
        Query query = em.createNamedQuery("Patient.findByPatientId");
        query.setParameter("patientId", patientId);
        return (Patient) query.getSingleResult();
    }

    public void create(Patient patient) {
        em.persist(patient);
    }

    public void update(Patient patient) {
        em.merge(patient);
    }
}
