package com.dedalus.restapi.resources;

import com.dedalus.restapi.dao.PatientDAO;
import com.dedalus.restapi.dto.PatientDto;
import com.dedalus.restapi.model.Patient;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("patients")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PatientResource {

    @Inject
    private PatientDAO patientDAO;

    @GET
    public Response getAll() {
        return Response.ok(patientDAO.getAll()).build();
    }

    @GET
    @Path("{id}")
    public Response getPatient(@PathParam("id") Long id) {
        try {
            Patient patient = patientDAO.findByPatientId(id);
            return Response.ok(patient).build();
        }catch (Exception e){
            return Response.status(404).build();
        }
    }

    private static final String REST_URI = "https://fhir-test.hl7.at/baseDstu2/Patient";

    private final Client client = ClientBuilder.newClient();

    private Response getResponse(Long id) {
        return client
                .target(REST_URI)
                .path(String.valueOf(id))
                .request()
                .accept("application/fhir+json")
                .get();
    }

    @POST
    @Path("{id}")
    public Response create(@PathParam("id") Long id) {
        Response response = getResponse(id);
        try {
            if (response.getStatus() == 200) {
                PatientDto patientData = response.readEntity(PatientDto.class);

                Patient patient;

                try{
                    patient = patientDAO.findByPatientId(id);
                } catch (Exception e){
                    patient = new Patient();
                }

                patient.setPatientId(id);
                patient.setBirthDate(patientData.getBirthDate());
                patient.setGender(patientData.getGender());
                if(patientData.getName()!=null) {
                    if(patientData.getName().get(0).getFamily()!=null)
                        patient.setFamily(patientData.getName().get(0).getFamily().get(0));
                    if(patientData.getName().get(0).getGiven()!=null)
                        patient.setGiven(patientData.getName().get(0).getGiven().get(0));
                    if(patientData.getName().get(0).getPrefix()!=null)
                        patient.setPrefix(patientData.getName().get(0).getPrefix().get(0));
                    if(patientData.getName().get(0).getSuffix()!=null)
                        patient.setSuffix(patientData.getName().get(0).getSuffix().get(0));
                }

                patientDAO.update(patient);

                return Response.ok("Created").build();
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
            return Response.status(500).build();
        } finally {
            response.close();
        }

        return Response.status(404).build();
    }
}
